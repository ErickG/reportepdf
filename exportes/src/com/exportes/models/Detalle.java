package com.exportes.models;
// Generated 12-18-2019 03:52:46 PM by Hibernate Tools 5.2.12.Final

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Detalle generated by hbm2java
 */
@Entity
@Table(name = "detalle", catalog = "facturacion")
public class Detalle implements java.io.Serializable {

	private DetalleId id;
	private int cantidad;
	private double precio;
	private int idProducto;

	public Detalle() {
	}

	public Detalle(DetalleId id, int cantidad, double precio, int idProducto) {
		this.id = id;
		this.cantidad = cantidad;
		this.precio = precio;
		this.idProducto = idProducto;
	}

	@EmbeddedId

	@AttributeOverrides({
			@AttributeOverride(name = "numDetalle", column = @Column(name = "num_detalle", nullable = false)),
			@AttributeOverride(name = "numFactura", column = @Column(name = "num_factura", nullable = false)) })
	public DetalleId getId() {
		return this.id;
	}

	public void setId(DetalleId id) {
		this.id = id;
	}

	@Column(name = "cantidad", nullable = false)
	public int getCantidad() {
		return this.cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	@Column(name = "precio", nullable = false, precision = 22, scale = 0)
	public double getPrecio() {
		return this.precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	@Column(name = "id_producto", nullable = false)
	public int getIdProducto() {
		return this.idProducto;
	}

	public void setIdProducto(int idProducto) {
		this.idProducto = idProducto;
	}

}
