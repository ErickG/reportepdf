/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exportes.config;

import com.exportes.dao.DetalleD;
import com.exportes.util.Dao;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author erick.gomezusam
 */
@Configuration
@ComponentScan(basePackages = {"com.exportes"})
public class Config {
    
    @Bean
    public Dao daoDetalle(){
        return new DetalleD();
    }
}
