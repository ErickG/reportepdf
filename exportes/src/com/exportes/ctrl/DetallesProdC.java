/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exportes.ctrl;

import com.exportes.util.Dao;
import com.exportes.util.Enlace;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperRunManager;
import org.springframework.stereotype.Component;

/**
 *
 * @author erick.gomezusam
 */
@Component
@RequestScoped
@ManagedBean(name="DetalleBean")
public class DetallesProdC implements Serializable {
    
    @ManagedProperty(value = "#{daoDetalle}")
    
    private Dao daoDetalle;

    public Dao getDaoDetalle() {
        return daoDetalle;
    }

    public void setDaoDetalle(Dao daoDetalle) {
        this.daoDetalle = daoDetalle;
    }

    public DetallesProdC() {
    }
    
    public void mostrarDetalles() throws JRException, IOException {
        File jasper = new File(FacesContext.getCurrentInstance().getExternalContext().getRealPath("reportes/Reporte.jasper"));
        Map<String, Object> parametros = new HashMap<String, Object>();
        parametros.put("parametro", "cualquiera");
        JasperPrint jasperprint = JasperFillManager.fillReport(jasper.getPath(), parametros, Enlace.conecta());
        //visualizar en el navegador
//        byte[] bytes = JasperRunManager.runReportToPdf(jasper.getPath(), parametros, Enlace.conecta());
//        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
//        response.setContentType("application/pdf");
//        response.setContentLength(bytes.length);
//        ServletOutputStream os = response.getOutputStream();
//        os.write(bytes, 0, bytes.length);
//        os.flush();
//        os.close();
//        FacesContext.getCurrentInstance().responseComplete();
        
        //descarga el pdf
        HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext().getResponse();
        response.addHeader("Content-disposition", "attachment; filename=reporte.pdf");
        ServletOutputStream printer = response.getOutputStream();
        JasperExportManager.exportReportToPdfStream(jasperprint, printer);
        FacesContext.getCurrentInstance().responseComplete();
    }
}
