/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exportes.util;

import java.util.List;

/**
 *
 * @author erick.gomezusam
 */
public interface Dao<T> {
    public void create(T t);
    public List<T> read();
    public T readBy(long id);
    public void update(T t);
    public void delete(T t);
}
