/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exportes.config;

import com.sun.faces.config.FacesInitializer;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.springframework.web.WebApplicationInitializer;
import org.springframework.web.context.ContextLoaderListener;
import org.springframework.web.context.support.AnnotationConfigWebApplicationContext;

/**
 *
 * @author erick.gomezusam
 */
public class WebIni extends FacesInitializer implements WebApplicationInitializer {

    @Override
    public void onStartup(ServletContext sc) throws ServletException {
        
        final AnnotationConfigWebApplicationContext root= new AnnotationConfigWebApplicationContext();
        root.register(Config.class);
        root.setServletContext(sc);
        sc.addListener(new ContextLoaderListener(root));
    }
    
}
