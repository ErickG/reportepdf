/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.exportes.util;

import java.sql.Connection;
import java.sql.DriverManager;

/**
 *
 * @author erick.gomezusam
 */
public class Enlace {
    private static Connection con;
    
    public static Connection conecta(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            con = DriverManager.getConnection("jdbc:mysql://usam-sql.sv.cds:3306/facturacion?useSSL=false", "kz", "kzroot");
        } catch (Exception e) {
            con=null;
        }
        return con;
    }
    public static void cierra(){
        try {
            if (con !=null) {
                if (!con.isClosed()) {
                    con.close();
                }
            }
        } catch (Exception e) {
            System.out.println("Error al cerrar la conexion");
        }
    }
}
